#!/usr/bin/env -S python3 -u

import socket
import struct
import logging
import argparse

from PeiDissector import PeiDissector, Status
from StatusForwarder import StatusForwarder

parser = argparse.ArgumentParser(
                    description=('Receive TETRA-PEI messages from IP'
                                 ' multicast and dissect, log and'
                                 ' optionally forward them.'))

parser.add_argument('--mcast_group', '-m', default='226.0.0.14',
                    help='Multicast group to listen on')
parser.add_argument('--mcast_port', '-p', type=int, default=10800,
                    help='Multicast port to listen on')
parser.add_argument('--listen_address', '-l', default='192.168.0.221',
                    help='Address of the interface to listen on')
parser.add_argument('--debug', '-d', default=False, action='store_true',
                    help='Show additional information')
parser.add_argument('--fward_iface', '-i', default=None,
                    help='Interface name for forwarding')
parser.add_argument('--fward_group', '-f', default=None,
                    help='Multicast group for forwarding')
parser.add_argument('--fward_ttl', '-t', default=2, type=int,
                    help='Time-to-live for forwarded messages')
parser.add_argument('--log-path', default='pei.log',
                    help='Log path')
parser.add_argument('--status-url', default=None, help='Status URL')
parser.add_argument('--status-secret', default=None, help='Shared secret')
args = parser.parse_args()

# Prepare logging
logger = logging.getLogger('tetra-pei')
logger.setLevel(logging.DEBUG if args.debug else logging.INFO)
formatter = logging.Formatter('%(asctime)s %(message)s')

streamHandler = logging.StreamHandler()
streamHandler.setLevel(logging.DEBUG)
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

if args.log_path:
    fileHandler = logging.FileHandler(args.log_path)
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)

logger.info('Starting up...')

# Create socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)

# Reuse address after closing
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Bind socket to listening port
sock.bind(('', args.mcast_port))

# Tell the kernel that we are interested in the given mcast group
mcast_addr = socket.inet_aton(args.mcast_group)
listen_addr = socket.inet_aton(args.listen_address)
mreq = struct.pack('4s4s', mcast_addr, listen_addr)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

# Prepare forwarding socket
if args.fward_group:
    fwd_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM,
                             socket.IPPROTO_UDP)
    fwd_sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL,
                        args.fward_ttl)
    if args.fward_iface:
        fwd_sock.setsockopt(socket.SOL_SOCKET, 25,
                            str(args.fward_iface + '\0').encode('utf-8'))

# Prepare Status forwarder
statusForwarder = None
if args.status_url:
    statusForwarder = StatusForwarder(logger, args.status_url,
                                      args.status_secret)

logger.info('Listening...')

dissector = PeiDissector(logger)

buf = ''

while True:
    logger.debug('buf before receive: ' + repr(buf))

    data, addr = sock.recvfrom(1024)
    logger.debug('received: ' + repr(data))

    if args.fward_group:
        ret = fwd_sock.sendto(data, (args.fward_group, args.mcast_port))
        logger.debug('forwarded: ' + repr(ret))

    try:
        buf += data.decode('ascii')
    except UnicodeError as e:
        logger.error(f'Failed to decode {repr(data)} from UTF-8: {e}')
        continue

    logger.debug('buf after receive: ' + repr(buf))

    idx = buf.find('\n')
    while idx > -1:
        line = buf[:idx].strip()
        buf = buf[idx + 1:]

        result = dissector.receive(line)
        if result:
            logger.info(result)

            if isinstance(result, Status) and statusForwarder:
                statusForwarder.propagate(result)

        idx = buf.find('\n')
