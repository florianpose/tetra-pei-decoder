# ----------------------------------------------------------------------------

import datetime
import requests

# ----------------------------------------------------------------------------

"""
{
    "parameters": {
        "address": "FW KLV Pressesprecher",
        "alarmType": "STATUS",
        "autoTriggered": "true",
        "clock_long": "17:08:57",
        "keyword": "2️⃣ 2 - Einsatzbereit auf Wache",
        "longdescription": "KLV Pressesprecher",
        "message": "Wechsel auf Status 2 für Fahrzeug KLV Pressesprecher",
        "name": "KLV Pressesprecher",
        "origin": "Wechsel auf Status 2 für Fahrzeug KLV Pressesprecher",
        "pluginmessage": "Wechsel auf Status 2 für Fahrzeug KLV Presse",
        "status": "2",
        "statusEmoji": "2️⃣",
        "statusSource": "C4",
        "statusText": "2 - Einsatzbereit auf Wache",
        "time": "17:08",
        "timestamp": "1609690137780",
        "unit": "Status",
        "unitCode": "b47b79a5-e772-4a2d-a00c-86c2da7e4e48",
        "user": "b47b79a5-e772-4a2d-a00c-86c2da7e4e48;",
        "userName": "Admin"
    }
}
"""


# ----------------------------------------------------------------------------

class StatusForwarder:

    forwardable = (1, 2, 3, 4, 6)

    def __init__(self, logger, url, secret):
        self.logger = logger
        self.url = url
        self.secret = secret

    def propagate(self, result):

        address = result.mt.name
        if not address:
            # ignore status for unknown terminals
            self.logger.debug('Not propagating status for unknown terminal.')
            return

        status = result.status
        if status not in self.forwardable:
            self.logger.debug('Not propagating status %i.', status)
            # ignore non-forwardable status
            return

        params = {
            "statusSource": "TETRA-PEI-LOGGER",
            "alarmType": "STATUS"
        }

        params["address"] = address
        params["status"] = str(status)
        dt = datetime.datetime.now()
        params["timestamp"] = \
            dt.strftime('%s') + '%03i' % int(dt.microsecond / 1000)

        data = {}
        data["parameters"] = params

        headers = {}
        if self.secret:
            headers["SHAREDSECRET"] = self.secret

        try:
            response = requests.post(self.url, headers=headers, json=data)
            self.logger.info('Propagated status: %s', response.text)
        except Exception as e:
            self.logger.error('Failed to propagate status: %s', e)

# ----------------------------------------------------------------------------
