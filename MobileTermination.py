import csv

# ----------------------------------------------------------------------------


class MobileTermination:

    names = {}

    @classmethod
    def read_csv(cls, path='fugs.csv'):
        with open(path, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                cls.names[row['TSI']] = row['Opta']

    def __init__(self, identifier, name=None):

        if not name:
            if not self.names:
                self.read_csv()
            if identifier in self.names:
                name = self.names[identifier]

        self.identifier = identifier
        self.name = name

    def __repr__(self):
        return f'{self.identifier} {self.name}'

# ----------------------------------------------------------------------------
