# Tetra PEI Decoder

Decode Tetra PEI messages from an UDP multicast source.

See [TETRA PEI Documentation](https://www.etsi.org/deliver/etsi_en/300300_300399/30039205/01.03.00_40/en_30039205v010300o.pdf).
See [Location Information Protocol](https://www.etsi.org/deliver/etsi_ts/100300_100399/1003921801/01.01.01_60/ts_1003921801v010101p.pdf).

# Supported Commands

## TETRA SDS Receive +CTSDSR

```
+CTSDSR: <AI service>, [<calling party identity>], [<calling party identity type>], <called party identity>, <called party identity type>, <length><CR><LF> user data.
```

Example:

```
+CTSDSR: 108,262100105437xxx,1,262100102440xxx,1,88,1
0A0046498A4BE418000040
```

User data in binary representation:

```
000010100000000001000110010010011000101001001011111001000001100000000000000000000100000
```

* Protocol Identifier: 00001010 (10 dez.) = Location Information Protocol
See [ETSI EN 300 392-2 V3.4.1](https:/www.etsi.org/deliver/etsi_en/300300_300399/30039202/03.04.01_60/en_30039202v030401p.pdf).

* PDU type (2 bit): 00
0: Short Location Report PDU
1: Long Location report PDU

* Time Elapsed (2 bit, M): 00

* Longitude (25 bit, M)

```python
>>> lon = bitstring.BitArray(bin="0000010001100100100110001")
>>> lon.int * 360.0 / 2**25
6.177588701248169
```

* Latitude (24 bit, M)

```python
>>> lat = bitstring.BitArray(bin="010010010111110010000011")
>>> lat.int * 180.0 / 2**24
51.670106649398804
```

* Position Error (3 bit, M): 000

* Horizontal Velocity (7 bit, M): 0000000

* Direction of Travel (4 bit, M): 0000

* Type of additional data (1 bit, M): 0
0: Reason for sending
1: User-defined

* Reason for sending (8 bit, C): 00000100 (4 dec.) = Status

* Remainder: 000
