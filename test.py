#!/usr/bin/env python3

import unittest
import logging
from PeiDissector import PeiDissector
from MobileTermination import MobileTermination

logger = logging.getLogger('tetra-pei-tests')
logger.setLevel(logging.CRITICAL)
# logger.setLevel(logging.DEBUG)

# ----------------------------------------------------------------------------


class DissectorTests(unittest.TestCase):

    def test_lip(self):
        MobileTermination.names = {'262100105434126': 'FW KLV 01 LF20 1'}
        lines = [
                '+CTSDSR: 108,262100105435431,1,262100102440320,1,88,1',
                '0A0049E3EA4CA0B051A820'
                ]
        dissector = PeiDissector(logger)
        self.assertEqual(dissector.receive(lines[0]), None)
        position = dissector.receive(lines[1])
        self.assertEqual(position.mt.identifier, '262100105435431')
        self.assertEqual(position.mt.name, None)
        self.assertEqual(position.lat, 51.73485517501831)
        self.assertEqual(position.lon, 6.494261026382446)

    def test_broken_lip(self):
        MobileTermination.names = {'262100105434126': 'FW KLV 01 LF20 1'}
        lines = [
                '+CTSDSR: 108,262100105435431,1,262100102440320,1,88,1',
                '0A0049E3E'
                ]
        dissector = PeiDissector(logger)
        self.assertEqual(dissector.receive(lines[0]), None)
        self.assertEqual(dissector.receive(lines[1]), None)

    def test_broken_lip2(self):
        lines = [
                '+CTSDSR: 108,262100105435431,1,262100102440320,1,40,1',
                '0A0049E3EA'
                ]
        dissector = PeiDissector(logger)
        self.assertEqual(dissector.receive(lines[0]), None)
        self.assertEqual(dissector.receive(lines[1]), None)

    def test_status(self):
        lines = [
                '+CTSDSR: 109,262100105434126,1,262100102440320,1,16',
                '8009'
                ]
        dissector = PeiDissector(logger)
        self.assertEqual(dissector.receive(lines[0]), None)
        status = dissector.receive(lines[1])
        self.assertEqual(status.mt.identifier, '262100105434126')
        self.assertEqual(status.status, 7)

    def test_name(self):
        MobileTermination.names = {'262100105434126': 'FW KLV 01 LF20 1'}
        lines = [
                '+CTSDSR: 109,262100105434126,1,262100102440320,1,16',
                '8009'
                ]
        dissector = PeiDissector(logger)
        self.assertEqual(dissector.receive(lines[0]), None)
        status = dissector.receive(lines[1])
        self.assertEqual(status.mt.identifier, '262100105434126')
        self.assertEqual(status.mt.name, 'FW KLV 01 LF20 1')

    def test_odd_hex_pdu_type_1(self):
        lines = [
                '+CTSDSR: 108,262100106437659,1,262100102440320,1,137',
                '0A4EA95C214119EEA92E1548088D69DE010'
                ]
        dissector = PeiDissector(logger)
        self.assertEqual(dissector.receive(lines[0]), None)
        position = dissector.receive(lines[1])
        self.assertEqual(position.mt.identifier, '262100106437659')
        self.assertEqual(position.mt.name, None)
        self.assertEqual(position.lat, 51.63756608963013)
        self.assertEqual(position.lon, 6.19479775428772)

    def test_with_null_byte(self):
        lines = [
            '+CTSDSR: 108,262100105435334,1,262100102440320,1,160,1',
            '0A4E4BD2E14120F2E92394678725FE004130\x0080E0'
        ]
        dissector = PeiDissector(logger)
        self.assertEqual(dissector.receive(lines[0]), None)
        position = dissector.receive(lines[1])
        self.assertEqual(position.mt.identifier, '262100105435334')
        self.assertEqual(position.mt.name, None)
        self.assertEqual(position.lat, 51.40677809715271)
        self.assertEqual(position.lon, 6.348971128463745)


# ----------------------------------------------------------------------------

if __name__ == '__main__':
    unittest.main()

# ----------------------------------------------------------------------------
